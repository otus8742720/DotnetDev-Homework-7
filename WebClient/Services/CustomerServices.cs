﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;

namespace WebClient.Services
{
    internal static class CustomerServices
    {
        private static Random random = new();
        private static string serverUri = GetServerUri();

        internal static string CreateManual()
        {
            Customer customer = GetCustomerFromConsole();
            using StringContent jsonContent = new(JsonSerializer.Serialize(new
            {
                Id = customer.Id,
                Firstname = customer.Firstname,
                Lastname = customer.Lastname
            }),
            Encoding.UTF8, "application/json");

            return "Id=" + customer.Id + ": " + PostCustomer(jsonContent);
        }

        internal static string PostCustomer(StringContent jsonContent)
        {
            using HttpClient client = new();
            try
            {
                using HttpResponseMessage response = client.PostAsync(serverUri, jsonContent).Result;
                return response.StatusCode.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        private static Customer GetCustomerFromConsole()
        {
            Console.Write("Введите Id (натуральное число): ");
            if (!int.TryParse(Console.ReadLine(), out int id) || id <= 0)
            {
                Console.WriteLine("Id будет присвоен автоматически");
                id = random.Next(0, 100);
            }
            Console.Write("Введите имя: ");
            string fName = Console.ReadLine();
            Console.Write("Введите фамилию: ");
            string lName = Console.ReadLine();

            if (String.IsNullOrWhiteSpace(fName) || String.IsNullOrWhiteSpace(lName))
            {
                Console.WriteLine("имя и фамилия не должны быть пустыми - попробуйте еще раз");
                return GetCustomerFromConsole();
            }
            return new Customer() { Id = id, Firstname = fName, Lastname = lName };
        }

        internal static string CreateRandom()
        {
            int id = random.Next(1, 100);
            using StringContent jsonContent = new(JsonSerializer.Serialize(new
            {
                Id = id,
                Firstname = RandomString(random.Next(5, 10)),
                Lastname = RandomString(random.Next(5, 10))
            }),
            Encoding.UTF8, "application/json");

            return "Id=" + id + ": " + PostCustomer(jsonContent);
        }

        internal static string GetById(int id)
        {
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };

            try
            {
                using HttpClient client = new();
                using HttpResponseMessage response = client.GetAsync(serverUri + id).Result;
                if (response.IsSuccessStatusCode)
                {
#nullable enable
                    Customer? customer = JsonSerializer.Deserialize<Customer>(response.Content.ReadAsStringAsync().Result, options);
#nullable disable
                    return "Found: " + customer?.Id + ": " + customer?.Firstname + " " + customer?.Lastname;
                }
                return "Failed to get customer by Id: " + response.StatusCode.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        internal static string GetAll()
        {
            using HttpClient client = new();
            try
            {
                using HttpResponseMessage response = client.GetAsync(serverUri).Result;
                return response.Content.ReadAsStringAsync().Result;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        static string GetServerUri()
        {
            try
            {
                IConfigurationBuilder builder = new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: false);

                IConfiguration config = builder.Build();
                return config["ServerUri"];
            }
            catch
            {
                return "https://localhost:5001/customers/";
            }
        }
    }
}
